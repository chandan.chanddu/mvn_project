properties([
    [$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', daysToKeepStr: '60', numToKeepStr: '60', artifactNumToKeepStr: '1']],
    disableConcurrentBuilds(),
    [$class: 'GithubProjectProperty', displayName: '', projectUrlStr: 'https://github.com/nuxeo/'],
    [$class: 'RebuildSettings', autoRebuild: false, rebuildDisabled: false],
    [$class: 'ParametersDefinitionProperty', parameterDefinitions: [
        [$class: 'StringParameterDefinition', defaultValue: 'master', description: '', name: 'BRANCH'],
        [$class: 'StringParameterDefinition', defaultValue: 'master', description: '', name: 'PARENT_BRANCH']]],
    pipelineTriggers([[$class: 'GitHubPushTrigger']])
  ])
  
  
	




def Projectname=${config.project}
def proxy=http://(proxy)
def repourl=${config.repo}
def dockertag=${config.tag}

	node('Build_Node') {

    try {
			stage('Checkout code') {
				steps {
					checkout scm
				}
			}
            stage ('build') {
                sh "${mvnHome}/bin/mvn clean install"
            }
			stage ('docker') {
                sh """
				docker build -t curl --build-arg http_proxy=${proxy} .
				docker tag ${repourl}:${dockertag}
				docker push ${repourl}:${dockertag}
				"""
            }
			
			stage ('docker') {
                sh """
				docker build -t curl --build-arg http_proxy=${proxy} .
				docker tag ${repourl}:${dockertag}
				docker push ${repourl}:${dockertag}
				"""
            }
            
    } catch(e) {
        currentBuild.result = "FAILURE"
        step([$class: 'ClaimPublisher'])
        throw e
    }
}
