Getting Started

This pipeline you can use to deploy the maven application in kubernetes environment. 

Prerequisites

get
1. the build node should have the valid kubeconfig of the deployment cluster so the cluster can be accessed from the build node for the deployment.
2. build node should have the maven installed and PATH should be set for it.
3. for docker tag you can use the git commit hash but here we are passing the variable from Jenkinsfile.
4. we need to check for the current deployment using a shell script and then use if condition for install or upgrade. for regular commits you can do the helm upgrade using the tag as overide for the deployment
